from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
from answer import *
import datetime

VALYA_FIRST = True
VALYA_ID = 373873903
# VALYA_ID = 139140467
TIME_VALYA = datetime.datetime.now().timestamp()
PERIOD = 28800


# CUSTOM FUNCTION

def get_chat_id(update):

    if update.message.from_user.id:
        return update.message.from_user.id
    else:
        return False


def check_valya(id):
    if int(id) == VALYA_ID:
        return True
    else:
        return False


def last_message_time():
    global TIME_VALYA
    global VALYA_FIRST
    if (datetime.datetime.now().timestamp() > TIME_VALYA + PERIOD) or (VALYA_FIRST == True):
        VALYA_FIRST = False
        TIME_VALYA = datetime.datetime.now().timestamp()
        return True
    return False

# END CUSTOM FUNCTION


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)

logger = logging.getLogger(__name__)





# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')


def echo(bot, update):

    if check_valya(get_chat_id(update)):
        if last_message_time():
            update.message.reply_text(Answer.hi_valya())
        else:
            if Answer.is_send():
                update.message.reply_text(Answer.get_words())
    else:
        if Answer.is_send():
            update.message.reply_text(Answer.get_words())



def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("771022219:AAEkfIdv8aLiWG9NCLZBiOCU09gxdLJARQ0")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
